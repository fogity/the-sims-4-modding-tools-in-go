// The thumbextractor tool extracts thumbnails for casparts.
package thumbextractor

import (
	"fmt"
	"os"
	"strings"

	"ts4libs/caspart"
	"ts4libs/consts"
	"ts4libs/dbpf"
	"ts4libs/keys"
	"ts4libs/thumbnail"
	"gopkg.in/qml.v1"
)

// Some error messages.
const (
	casPartFileMissing = "A Cas Part Package must be specified."
	thumbFileMissing   = "A Thumbnail Package must be specified."
	exportDirMissing   = "An Export Directory must be specified."
)

// trimPath removes the 'file' prefix added by qml.
func trimPath(path string) string {
	return strings.TrimPrefix(path, "file:/")
}

// Data is the tool state.
type Data struct {
	CasPartFile, ThumbFile, ExportDir, Information string
}

// inform updates the information text.
func (d *Data) inform(text string) {
	d.Information = text
	qml.Changed(d, &d.Information)
}

// report updates the information text with an error message.
func (d *Data) report(err error) {
	d.Information = err.Error()
	qml.Changed(d, &d.Information)
}

// Export performs the identification, extraction and conversion of the thumbnails.
func (d *Data) Export() {
	if d.CasPartFile == "" {
		d.inform(casPartFileMissing)
		return
	}

	if d.ThumbFile == "" {
		d.inform(thumbFileMissing)
		return
	}

	if d.ExportDir == "" {
		d.inform(exportDirMissing)
		return
	}

	casPartPack, err := dbpf.Open(trimPath(d.CasPartFile))
	if err != nil {
		d.report(err)
		return
	}

	thumbPack, err := dbpf.Open(trimPath(d.ThumbFile))
	if err != nil {
		d.report(err)
		return
	}

	folder := trimPath(d.ExportDir)

	casParts := make([]uint64, 0)
	casPartNames := make(map[uint64]string)
	for k, r := range casPartPack.ListResources(&keys.Filter{[]uint32{consts.ResourceTypeCasPart}, nil, nil}, nil, nil) {
		data, err := r.ToBytes()
		if err != nil {
			d.report(err)
			return
		}
		casPart, err := caspart.Read(data)
		if err != nil {
			d.report(err)
			return
		}
		casParts = append(casParts, k.Instance)
		casPartNames[k.Instance] = casPart.Name
	}

	count := 0
	for k, r := range thumbPack.ListResources(&keys.Filter{nil, []uint32{consts.ResourceGroupPortraitFemale, consts.ResourceGroupPortraitMale}, casParts}, nil, nil) {
		data, err := r.ToBytes()
		if err != nil {
			d.report(err)
			return
		}
		thumb, err := thumbnail.Convert(data)
		if err != nil {
			d.report(err)
			return
		}
		file, err := os.Create(fmt.Sprintf("%v/%v_%x.png", folder, casPartNames[k.Instance], k.Group))
		if err != nil {
			d.report(err)
			return
		}
		_, err = file.Write(thumb)
		if err != nil {
			d.report(err)
			file.Close()
			return
		}
		file.Close()
		count++
	}

	d.inform(fmt.Sprintf("Extraction completed, %v thumbnails extracted.", count))
}

// CreateWindow creates a new instance of the tool.
func CreateWindow() error {
	engine := qml.NewEngine()

	extractor, err := engine.LoadFile("qrc:///qml/thumbextractor/Window.qml")
	if err != nil {
		return err
	}

	context := engine.Context()
	d := new(Data)
	d.Information = "Enter files and press Extract"
	context.SetVar("app", d)

	window := extractor.CreateWindow(nil)
	window.Show()

	return nil
}
