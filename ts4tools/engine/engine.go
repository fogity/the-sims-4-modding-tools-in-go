// This program can be used to run scripts made in the ts4libs dsl.
package main

import (
	"fmt"
	"os"

	"ts4libs/script"
)

// main function.
func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Expecting 1 argument, found %v\n", len(os.Args)-1)
		return
	}

	fmt.Printf("Running script...\n")

	err := script.RunFile(os.Args[1])
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}

	fmt.Printf("Script run sucessfully.\n")
}
