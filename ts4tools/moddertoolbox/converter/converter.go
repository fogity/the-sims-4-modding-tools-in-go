// The converter tool allows conversion of large numbers between decimal and hexadecimal.
package converter

import (
	"fmt"
	"regexp"
	"strings"

	"gopkg.in/qml.v1"
)

const (
	// Formatting strings.
	formatHex32 = "0x%08X"
	formatHex64 = "0x%016X"
	formatDec32 = "%v"
	formatDec64 = "%v"

	// Some error messages.
	resultInputInvalid  = "Invalid input"
	resultInputTooLarge = "Number too large"
)

// Some regular expressions to validate input.
var (
	matchHex = regexp.MustCompile("^0x[0-9a-fA-F]+$")
	matchDec = regexp.MustCompile("^[0-9]+$")
)

// Data is the tool state.
type Data struct {
	Result     string
	AlignRight bool
}

// ChangeText is called when the input text changes.
func (d *Data) ChangeText(text string) {
	str := strings.TrimSpace(text)

	if str == "" {
		d.Update("", false)
		return
	}

	if matchHex.MatchString(str) {
		var ui32 uint32
		if _, err := fmt.Sscan(str, &ui32); err == nil {
			d.Update(fmt.Sprintf(formatDec32, ui32), true)
			return
		}

		var ui64 uint64
		if _, err := fmt.Sscan(str, &ui64); err == nil {
			d.Update(fmt.Sprintf(formatDec64, ui64), true)
			return
		}

		d.Update(resultInputTooLarge, false)
		return
	}

	if matchDec.MatchString(str) {
		str = strings.TrimLeft(str, "0")
		if str == "" {
			str = "0"
		}

		var ui32 uint32
		if _, err := fmt.Sscan(str, &ui32); err == nil {
			d.Update(fmt.Sprintf(formatHex32, ui32), true)
			return
		}

		var ui64 uint64
		if _, err := fmt.Sscan(str, &ui64); err == nil {
			d.Update(fmt.Sprintf(formatHex64, ui64), true)
			return
		}

		d.Update(resultInputTooLarge, false)
		return
	}

	d.Update(resultInputInvalid, false)
}

// Update updates the ui with the new result.
func (d *Data) Update(result string, alignRight bool) {
	d.Result = result
	d.AlignRight = alignRight
	qml.Changed(d, &d.Result)
}

// CreateWindow creates a new instance of the tool.
func CreateWindow() error {
	engine := qml.NewEngine()

	converter, err := engine.LoadFile("qrc:///qml/converter/Window.qml")
	if err != nil {
		return err
	}

	context := engine.Context()
	context.SetVar("app", new(Data))

	window := converter.CreateWindow(nil)
	window.Show()

	return nil
}
