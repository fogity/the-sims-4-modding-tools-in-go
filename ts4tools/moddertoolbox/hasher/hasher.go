// The hasher tool allows hashing of strings into some different fnv hashes.
package hasher

import (
	"fmt"

	"ts4libs/hash"
	"gopkg.in/qml.v1"
)

const (
	// The names of the number formats.
	numberFormatHex = "hex"
	numberFormatDec = "dec"

	// Formatting strings.
	formatHex32 = "0x%08X"
	formatHex64 = "0x%016X"
	formatDec32 = "%v"
	formatDec64 = "%v"
)

// Data is the tool state.
type Data struct {
	Fnv24, Fnv32, Fnv32High, Fnv64, Fnv64High string
	Text, Format                              string
}

// ChangeText is called when the input text changes.
func (d *Data) ChangeText(text string) {
	d.Text = text
	d.Calculate()
}

// ChangeFormat is called when the number format is changed.
func (d *Data) ChangeFormat(format string) {
	d.Format = format
	d.Calculate()
}

// Calculate computes the new values and updates the ui.
func (d *Data) Calculate() {
	var f32, f64 string

	switch d.Format {
	case numberFormatHex:
		f32 = formatHex32
		f64 = formatHex64
	default:
		f32 = formatDec32
		f64 = formatDec64
	}

	ui32 := hash.Fnv24(d.Text)
	d.Fnv24 = fmt.Sprintf(f32, ui32)
	qml.Changed(d, &d.Fnv24)

	ui32 = hash.Fnv32(d.Text)
	d.Fnv32 = fmt.Sprintf(f32, ui32)
	qml.Changed(d, &d.Fnv32)

	ui32 = hash.Fnv32HighBit(d.Text)
	d.Fnv32High = fmt.Sprintf(f32, ui32)
	qml.Changed(d, &d.Fnv32High)

	ui64 := hash.Fnv64(d.Text)
	d.Fnv64 = fmt.Sprintf(f64, ui64)
	qml.Changed(d, &d.Fnv64)

	ui64 = hash.Fnv64HighBit(d.Text)
	d.Fnv64High = fmt.Sprintf(f64, ui64)
	qml.Changed(d, &d.Fnv64High)
}

// CreateWindow creates a new instance of the tool.
func CreateWindow() error {
	engine := qml.NewEngine()

	hasher, err := engine.LoadFile("qrc:///qml/hasher/Window.qml")
	if err != nil {
		return err
	}

	context := engine.Context()

	d := new(Data)
	d.ChangeFormat(numberFormatHex)
	context.SetVar("app", d)

	window := hasher.CreateWindow(nil)
	window.Show()

	return nil
}
