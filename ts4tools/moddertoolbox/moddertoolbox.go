// This program contains a few tools useful when modding.
package main

// Instructions for the compiler to generate qml code.
//go:generate genqrc qml

import (
	"fmt"
	"os"

	"ts4tools/moddertoolbox/converter"
	"ts4tools/moddertoolbox/hasher"
	"gopkg.in/qml.v1"
)

// App is the state of the program.
// It does not need any state.
type App struct{}

// Create is called when a new tool should be launched.
func (*App) Create(tool string) {
	switch tool {
	case "hasher":
		hasher.CreateWindow()
	case "converter":
		converter.CreateWindow()
	}
}

// main function.
func main() {
	if err := qml.Run(run); err != nil {
		fmt.Printf("error: %v\n", err)
		os.Exit(1)
	}
}

// run starts the program ui.
func run() error {
	engine := qml.NewEngine()

	engine.On("quit", func() { os.Exit(0) })

	toolbox, err := engine.LoadFile("qrc:///qml/Window.qml")
	if err != nil {
		return err
	}

	context := engine.Context()
	context.SetVar("app", new(App))

	window := toolbox.CreateWindow(nil)
	window.Show()
	window.Wait()

	return nil
}
