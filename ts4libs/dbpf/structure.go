// Package dbpf handles database package files.
// This file defines the package structure.
package dbpf

import (
	"os"
)

// Package is the main dbpf object.
type Package struct {
	// The header of the file.
	header    header
	// The resource record.
	record    record
	// A pointer to the actual file object.
	file      *os.File
	// A list of resource objects beloning to the file.
	resources []*Resource
}

// Version is used for versioning a file.
type Version struct {
	Major, Minor uint32
}

// header is the header of the file.
type header struct {
	// The file type identifier.
	Identifier        [4]byte
	// The version of the file format.
	FileVersion       Version
	// The version of this file.
	UserVersion       Version
	_                 uint32
	// Timestamps.
	CreationTime      uint32
	UpdateTime        uint32
	_                 uint32
	// The number of resource entries.
	EntryCount        uint32
	// The old record position field.
	RecordPositionLow uint32
	// The size of the record.
	RecordSize        uint32
	_, _, _           uint32
	Unused            uint32
	// The new record position field.
	RecordPosition    uint64
	_, _, _, _, _, _  uint32
}

// record contains the resource information.
type record struct {
	Flags      uint32
	Type       uint32
	Group      uint32
	InstanceEx uint32
	Entries    []*entry
}

// entry contains information for a resource.
type entry struct {
	Type       uint32
	Group      uint32
	InstanceEx uint32
	Fixed      entryFixed
	Extended   entryExtended
}

// entryFixed contains the fixed part of the entry.
type entryFixed struct {
	Instance         uint32
	Position         uint32
	CompressedSize   uint32
	DecompressedSize uint32
}

// entryExtended contains the parts added with an extended compression type.
type entryExtended struct {
	CompressionType uint16
	Committed       uint16
}
