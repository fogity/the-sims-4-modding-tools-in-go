// Package dbpf handles database package files.
// This file contains the package file API.
package dbpf

import (
	"fmt"

	"ts4libs/keys"
)

// The supported dbpf version.
var (
	sims4Version = Version{2, 1}
)

// Open loads a package file given a file path.
func Open(path string) (*Package, error) {
	var p Package
	err := p.readFile(path)
	if err != nil {
		return nil, err
	}
	p.loadResourceList()
	return &p, nil
}

// New creates an empty package file.
func New() *Package {
	var p Package
	p.initFile(sims4Version)
	p.resources = make([]*Resource, 0)
	return &p
}

// Save writes the package file to disk.
func (p *Package) Save() error {
	if p.file == nil {
		return fmt.Errorf("no file associated with package")
	}
	p.saveResourceList()
	return p.writeFile(p.file.Name())
}

// SaveAs writes the package file to a given file path.
func (p *Package) SaveAs(path string) error {
	err := p.saveResourceList()
	if err != nil {
		return err
	}
	return p.writeFile(path)
}

// Close closes the underlaying file.
func (p *Package) Close() {
	p.file.Close()
	p.file = nil
}

// AddResource inserts a resource into the package file.
func (p *Package) AddResource(resource *Resource) {
	for i, r := range p.resources {
		if r.key == resource.key {
			p.resources[i] = resource
			return
		}
	}
	p.resources = append(p.resources, resource)
}

// ListResources returns a key resource map for the package file based on the query.
// First argument is a whitelisting filter.
// Second argument is a blacklisting filter.
// Third argument is an existing map to insert the resources into.
// All arguments may be nil.
func (p *Package) ListResources(include, exclude *keys.Filter, resources map[keys.Key]*Resource) map[keys.Key]*Resource {
	if resources == nil {
		resources = make(map[keys.Key]*Resource)
	}

	for _, resource := range p.resources {
		if include != nil {
			if !include.Include(resource) {
				continue
			}
		}
		if exclude != nil {
			if exclude.Exclude(resource) {
				continue
			}
		}
		resources[resource.key] = resource
	}

	return resources
}
