// Package dbpf handles database package files.
// This file contains the IO.
package dbpf

import (
	"encoding/binary"
	"io/ioutil"
	"os"
)

const (
	// An unused field that should still be 3.
	unused                  = 3
	// Flags for the Flags field.
	constantType            = 1 << 0
	constantGroup           = 1 << 1
	constantInstanceEx      = 1 << 2
	// Flag for field extensions.
	extendedCompressionType = 1 << (32 - 1)
)

var (
	// The file identifier.
	identifier = [4]byte{'D', 'B', 'P', 'F'}
	// The size of the header fields.
	headerSize = uint64(binary.Size(header{}))
)

// initFile sets up a clean header.
func (p *Package) initFile(version Version) {
	p.header.Identifier = identifier
	p.header.FileVersion = version
	p.header.Unused = unused
	p.header.RecordPosition = headerSize
	p.record.Flags = constantType | constantGroup | constantInstanceEx
}

// readFile reads a file from disk.
func (p *Package) readFile(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	p.file = file
	err = binary.Read(file, binary.LittleEndian, &p.header)
	if err != nil {
		return err
	}
	err = p.readRecord(file)
	if err != nil {
		return err
	}
	return nil
}

// writeFile writes a file to disk.
func (p *Package) writeFile(path string) error {
	// A temp file is used to prevent read conflicts.
	temp, err := ioutil.TempFile(".", "dbpf-")
	if err != nil {
		return err
	}
	err = binary.Write(temp, binary.LittleEndian, &p.header)
	if err != nil {
		return err
	}
	for _, resoruce := range p.resources {
		if resoruce.comp != nil {
			err = binary.Write(temp, binary.LittleEndian, resoruce.comp)
			if err != nil {
				return err
			}
		}
	}
	err = p.writeRecord(temp)
	if err != nil {
		return err
	}
	temp.Close()
	// Close the original file so it may be released (and maybe deleted).
	p.file.Close()
	// Delete any existing file with the path.
	err = os.RemoveAll(path)
	// If it can't be deleted, reopen and report.
	if err != nil {
		file, _ := os.Open(p.file.Name())
		p.file = file
		return err
	}
	// Rename the temp file to the new path.
	err = os.Rename(temp.Name(), path)
	// If it can't be renamed, reopen and report.
	if err != nil {
		file, _ := os.Open(p.file.Name())
		p.file = file
		return err
	}
	// Finally open the new file in the new path.
	file, err := os.Open(path)
	p.file = file
	return err
}

// readRecord reads the resource record.
func (p *Package) readRecord(file *os.File) error {
	_, err := file.Seek(int64(p.header.RecordPosition), os.SEEK_SET)
	if err != nil {
		return err
	}
	var flags uint32
	err = binary.Read(file, binary.LittleEndian, &flags)
	if err != nil {
		return err
	}
	p.record.Flags = flags
	if (flags & constantType) != 0 {
		err = binary.Read(file, binary.LittleEndian, &p.record.Type)
		if err != nil {
			return err
		}
	}
	if (flags & constantGroup) != 0 {
		err = binary.Read(file, binary.LittleEndian, &p.record.Group)
		if err != nil {
			return err
		}
	}
	if (flags & constantInstanceEx) != 0 {
		err = binary.Read(file, binary.LittleEndian, &p.record.InstanceEx)
		if err != nil {
			return err
		}
	}
	p.record.Entries = make([]*entry, p.header.EntryCount)
	for i := range p.record.Entries {
		entry, err := p.readEntry(file, flags)
		if err != nil {
			return err
		}
		p.record.Entries[i] = entry
	}
	return nil
}

// writeRecord writes the resource record.
func (p *Package) writeRecord(file *os.File) error {
	_, err := file.Seek(int64(p.header.RecordPosition), os.SEEK_SET)
	if err != nil {
		return err
	}
	flags := p.record.Flags
	err = binary.Write(file, binary.LittleEndian, &flags)
	if err != nil {
		return err
	}
	if (flags & constantType) != 0 {
		err = binary.Write(file, binary.LittleEndian, &p.record.Type)
		if err != nil {
			return err
		}
	}
	if (flags & constantGroup) != 0 {
		err = binary.Write(file, binary.LittleEndian, &p.record.Group)
		if err != nil {
			return err
		}
	}
	if (flags & constantInstanceEx) != 0 {
		err = binary.Write(file, binary.LittleEndian, &p.record.InstanceEx)
		if err != nil {
			return err
		}
	}
	for _, entry := range p.record.Entries {
		err = p.writeEntry(file, flags, entry)
		if err != nil {
			return err
		}
	}
	return nil
}

// readEntry reads a resource record entry.
func (p *Package) readEntry(file *os.File, flags uint32) (*entry, error) {
	var entry entry
	if (flags & constantType) == 0 {
		err := binary.Read(file, binary.LittleEndian, &entry.Type)
		if err != nil {
			return nil, err
		}
	}
	if (flags & constantGroup) == 0 {
		err := binary.Read(file, binary.LittleEndian, &entry.Group)
		if err != nil {
			return nil, err
		}
	}
	if (flags & constantInstanceEx) == 0 {
		err := binary.Read(file, binary.LittleEndian, &entry.InstanceEx)
		if err != nil {
			return nil, err
		}
	}
	err := binary.Read(file, binary.LittleEndian, &entry.Fixed)
	if err != nil {
		return nil, err
	}
	if (entry.Fixed.CompressedSize & extendedCompressionType) != 0 {
		err = binary.Read(file, binary.LittleEndian, &entry.Extended)
		if err != nil {
			return nil, err
		}
	}
	return &entry, nil
}

// writeEntry writes a resource record entry.
func (p *Package) writeEntry(file *os.File, flags uint32, entry *entry) error {
	if (flags & constantType) == 0 {
		err := binary.Write(file, binary.LittleEndian, &entry.Type)
		if err != nil {
			return err
		}
	}
	if (flags & constantGroup) == 0 {
		err := binary.Write(file, binary.LittleEndian, &entry.Group)
		if err != nil {
			return err
		}
	}
	if (flags & constantInstanceEx) == 0 {
		err := binary.Write(file, binary.LittleEndian, &entry.InstanceEx)
		if err != nil {
			return err
		}
	}
	err := binary.Write(file, binary.LittleEndian, &entry.Fixed)
	if err != nil {
		return err
	}
	if (entry.Fixed.CompressedSize & extendedCompressionType) != 0 {
		err = binary.Write(file, binary.LittleEndian, &entry.Extended)
		if err != nil {
			return err
		}
	}
	return nil
}
