// Package keys defines resource keys and filters.
// This file defines the keys.
package keys

// Key is an identifier for a resource.
type Key struct {
	Type, Group uint32
	Instance    uint64
}

// Keyer is an interface for objects identified by a key.
type Keyer interface {
	Key() Key
}

// CombineKey creates a key given type, group and the instance divided into two parts.
func CombineKey(t, g, ie, i uint32) Key {
	return Key{t, g, (uint64(ie) << 32) + uint64(i)}
}
