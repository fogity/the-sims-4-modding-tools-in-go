// Package keys defines resource keys and filters.
// This file defines the filters.
package keys

// Filter is an object that can filter based on keys.
type Filter struct {
	Types, Groups []uint32
	Instances     []uint64
}

// Include determines if an object should be included based on its key.
func (f *Filter) Include(k Keyer) bool {
	key := k.Key()

	if f.Types != nil {
		ok := false
		for _, t := range f.Types {
			if t == key.Type {
				ok = true
			}
		}
		if !ok {
			return false
		}
	}

	if f.Groups != nil {
		ok := false
		for _, g := range f.Groups {
			if g == key.Group {
				ok = true
			}
		}
		if !ok {
			return false
		}
	}

	if f.Instances != nil {
		ok := false
		for _, i := range f.Instances {
			if i == key.Instance {
				ok = true
			}
		}
		if !ok {
			return false
		}
	}

	return true
}

// Exclude determines if an object should be excluded based on its key.
func (f *Filter) Exclude(k Keyer) bool {
	key := k.Key()

	if f.Types != nil {
		for _, t := range f.Types {
			if t == key.Type {
				return true
			}
		}
	}

	if f.Groups != nil {
		for _, g := range f.Groups {
			if g == key.Group {
				return true
			}
		}
	}

	if f.Instances != nil {
		for _, i := range f.Instances {
			if i == key.Instance {
				return true
			}
		}
	}

	return false
}

// MergeFilters combines two filters into a new filter.
func MergeFilters(a, b *Filter) *Filter {
	var types, groups []uint32
	var instances []uint64

	if a == nil {
		return b
	}

	if b == nil {
		return a
	}

	if a.Types == nil {
		types = b.Types
	} else if b.Types == nil {
		types = a.Types
	} else {
		types = merge32(a.Types, b.Types)
	}

	if a.Groups == nil {
		groups = b.Groups
	} else if b.Groups == nil {
		groups = a.Groups
	} else {
		groups = merge32(a.Groups, b.Groups)
	}

	if a.Instances == nil {
		instances = b.Instances
	} else if b.Instances == nil {
		instances = a.Instances
	} else {
		instances = merge64(a.Instances, b.Instances)
	}

	return &Filter{types, groups, instances}
}

// merge32 combines two uint32 lists without duplicates.
func merge32(a, b []uint32) []uint32 {
	merge := make([]uint32, 0)
	for _, n := range a {
		merge = append(merge, n)
	}
	for _, n := range b {
		ok := true
		for _, e := range merge {
			if e == n {
				ok = false
			}
		}
		if ok {
			merge = append(merge, n)
		}
	}
	return merge
}

// merge64 combines two uint64 lists without duplicates.
func merge64(a, b []uint64) []uint64 {
	merge := make([]uint64, 0)
	for _, n := range a {
		merge = append(merge, n)
	}
	for _, n := range b {
		ok := true
		for _, e := range merge {
			if e == n {
				ok = false
			}
		}
		if ok {
			merge = append(merge, n)
		}
	}
	return merge
}
