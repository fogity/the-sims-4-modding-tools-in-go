// Package hash contains hashing functions.
package hash

import (
	"hash/fnv"
	"strings"
)

const (
	// Masks.
	mask24    = (1 << 24) - 1
	// High bits.
	highBit32 = 1 << (32 - 1)
	highBit64 = 1 << (64 - 1)
)

// Fnv24 creates a 24 bit fnv hash of a string.
func Fnv24(s string) uint32 {
	hash := Fnv32(s)
	return (hash >> 24) ^ (hash & mask24)
}

// Fnv32 creates a 32 bit fnv hash of a string.
func Fnv32(s string) uint32 {
	hash := fnv.New32()
	hash.Write([]byte(strings.ToLower(s)))
	return hash.Sum32()
}

// Fnv32 creates a 32 bit fnv hash of a string and sets the high bit.
func Fnv32HighBit(s string) uint32 {
	return Fnv32(s) | highBit32
}

// Fnv64 creates a 64 bit fnv hash of a string.
func Fnv64(s string) uint64 {
	hash := fnv.New64()
	hash.Write([]byte(strings.ToLower(s)))
	return hash.Sum64()
}

// Fnv64 creates a 64 bit fnv hash of a string and sets the high bit.
func Fnv64HighBit(s string) uint64 {
	return Fnv64(s) | highBit64
}
