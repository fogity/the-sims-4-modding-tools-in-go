// Package script implements a domain specific language for making mods.
// This file contains convenient functions for running scripts.
package script

import "golibs/script"

// Run executes a script given as bytes.
func Run(data []byte) error {
	return script.Run(definitions, data)
}

// RunFile executes a script given its file path.
func RunFile(path string) error {
	return script.RunFile(definitions, path)
}
