// Package script implements a domain specific language for making mods.
// This file defines a useful proxy for package files.
package script

import (
	"ts4libs/dbpf"
	"ts4libs/keys"
)

// Package is a proxy for a package object.
type Package struct {
	// A pointer to the actual package object.
	p       *dbpf.Package
	// A list of packages that patches the package.
	merge   []*dbpf.Package
	// A whitelisting filter for resources.
	include *keys.Filter
	// A blacklisting filter for resources.
	exclude *keys.Filter
}

// CreatePackage creates a proxy to an empty package.
func CreatePackage() *Package {
	p := &Package{
		p: dbpf.New(),
	}
	return p
}

// OpenPackage loads a package and creates a proxy.
func OpenPackage(path string) (*Package, error) {
	pack, err := dbpf.Open(path)
	p := &Package{
		p: pack,
	}
	return p, err
}

// Merge loads a package and adds it as a patch in the proxy.
func (p *Package) Merge(path string) error {
	pack, err := dbpf.Open(path)
	if err != nil {
		return err
	}
	if p.merge == nil {
		p.merge = make([]*dbpf.Package, 0)
	}
	p.merge = append(p.merge, pack)
	return nil
}

// Include adds a filter to the whitelist.
func (p *Package) Include(filter *keys.Filter) {
	p.include = keys.MergeFilters(p.include, filter)
}

// Exclude adds a filter to the blacklist.
func (p *Package) Exclude(filter *keys.Filter) {
	p.exclude = keys.MergeFilters(p.exclude, filter)
}
