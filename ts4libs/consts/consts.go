// Package consts contains some useful constants.
package consts

// Some resource types.
const (
	ResourceTypeCasPart      = uint32(0x034AEECB)
	ResourceTypeTuningModule = uint32(0x03B33DDF)
)

// Some resource groups.
const (
	ResourceGroupPortraitFemale = uint32(0x2)
	ResourceGroupPortraitMale   = uint32(0x102)
)
