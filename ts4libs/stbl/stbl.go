// Package stbl handles string table files.
package stbl

import (
	"bytes"
	"encoding/binary"
)

// The supported format version.
const (
	sims4version = 5
)

// header is the header of the file.
type header struct {
	Identifier   [4]byte
	Version      uint16
	Compressed   byte
	NumEntries   uint64
	_            [2]byte
	StringLength uint32
}

// Table is the main string table object.
type Table struct {
	header  header
	Entries []Entry
}

// Entry is an entry in the string table.
type Entry struct {
	Key    uint32
	Flags  byte
	Length uint16
	String string
}

// Read reads a table from bytes.
func Read(data []byte) (*Table, error) {
	table := new(Table)
	r := bytes.NewReader(data)

	err := binary.Read(r, binary.LittleEndian, &table.header)
	if err != nil {
		return nil, err
	}

	table.Entries = make([]Entry, table.header.NumEntries)
	for i := range table.Entries {
		var entry Entry
		binary.Read(r, binary.LittleEndian, &entry.Key)
		binary.Read(r, binary.LittleEndian, &entry.Flags)
		binary.Read(r, binary.LittleEndian, &entry.Length)
		array := make([]byte, entry.Length)
		r.Read(array)
		entry.String = string(array)
		table.Entries[i] = entry
	}

	return table, nil
}
