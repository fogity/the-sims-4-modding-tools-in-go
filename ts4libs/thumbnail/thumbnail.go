// Package thumbnail converts jpeg images with an alpha mask into png images.
package thumbnail

import (
	"bytes"
	"encoding/binary"
	"errors"
	"image"
	"image/color"
	"image/jpeg"
	"image/png"
)

// Some new errors.
var (
	noMaskError      = errors.New("thumbnail: alpha mask not found")
	invalidMaskError = errors.New("thumbnail: alpha mask is not valid")
)

// extractMask extracts the alpha mask from the jpeg metadata.
func extractMask(data []byte) ([]byte, error) {
	for i := 0; i < len(data); i++ {
		if i+12 > len(data) {
			break
		}
		if data[i] != 0xFF {
			continue
		}
		if data[i+1] != 0xE0 {
			continue
		}
		if string(data[i+4:i+8]) != "ALFA" {
			continue
		}
		size := int(binary.BigEndian.Uint32(data[i+8 : i+12]))
		if i+12+size > len(data) {
			break
		}
		return data[i+12 : i+12+size], nil
	}
	return nil, noMaskError
}

// Convert takes a jpeg with an alpha mask and returns a png image.
func Convert(data []byte) ([]byte, error) {
	mask, err := extractMask(data)
	if err != nil {
		return nil, err
	}

	alpha, err := png.Decode(bytes.NewReader(mask))
	if err != nil {
		return nil, err
	}

	gray, ok := alpha.(*image.Gray)
	if !ok {
		return nil, invalidMaskError
	}

	colors, err := jpeg.Decode(bytes.NewReader(data))
	if err != nil {
		return nil, err
	}

	bounds := colors.Bounds()

	img := image.NewRGBA(bounds)
	for x := bounds.Min.X; x <= bounds.Max.X; x++ {
		for y := bounds.Min.Y; y <= bounds.Max.Y; y++ {
			r, g, b, _ := colors.At(x, y).RGBA()
			a := gray.GrayAt(x, y).Y
			img.SetRGBA(x, y, color.RGBA{uint8(r), uint8(g), uint8(b), a})
		}
	}

	buf := new(bytes.Buffer)
	err = png.Encode(buf, img)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}
