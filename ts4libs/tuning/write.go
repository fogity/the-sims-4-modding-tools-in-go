// Package tuning handles writing of tunable instances from a combined tuning.
package tuning

import (
	"errors"
	"fmt"
	"os"

	"ts4libs/tuning/combined"
)

// The header to use for the xml document.
const (
	xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
)

// A new error.
var (
	noFileError = errors.New("tuning: no file to write to")
)

// Context specifies how to write the tuning.
type Context struct {
	// The file to write to.
	File                       *os.File
	// The strings to use for indentation and line feeds.
	Indentation, LineEnd       string
	// Whether external references names should be added as comments.
	AddReferences              bool
	// Maps containing external reference names based on hashes.
	Strings, Tunings, CasParts map[int]string
}

// indent writes out indentation for the given depth.
func (c *Context) indent(depth int) {
	for i := 0; i < depth; i++ {
		c.File.WriteString(c.Indentation)
	}
}

// writeArgument writes out an xml tag attribute.
func (c *Context) writeArgument(argument, value string) {
	if len(value) > 0 {
		c.File.WriteString(fmt.Sprintf(" %v=\"%v\"", argument, value))
	}
}

// addReference writes out a reference comment for a tunable value.
func (c *Context) addReference(tunable combined.Tunable) {
	var i int
	_, err := fmt.Sscan(tunable.Value, &i)
	if err != nil {
		return
	}
	if tunable.Name == "cas_part" {
		part, ok := c.CasParts[i]
		if ok {
			c.File.WriteString(fmt.Sprintf("<!-- cas_part: %v -->", part))
		}
	} else {
		tuning, ok := c.Tunings[i]
		if ok {
			c.File.WriteString(fmt.Sprintf("<!-- tuning: %v -->", tuning))
		}
		str, ok := c.Strings[i]
		if ok {
			c.File.WriteString(fmt.Sprintf("<!-- string: %v -->", str))
		}
	}
}

// writeTunable writes out a tunable at a specified indentation depth.
func (c *Context) writeTunable(tunable combined.Tunable, depth int) {
	c.indent(depth)
	c.File.WriteString("<")
	c.File.WriteString(tunable.XMLName.Local)
	c.writeArgument("t", tunable.Type)
	c.writeArgument("p", tunable.Path)
	c.writeArgument("ev", tunable.Enum)
	c.writeArgument("n", tunable.Name)
	if len(tunable.Tunables) == 0 && len(tunable.Value) == 0 {
		c.File.WriteString(" />")
		c.File.WriteString(c.LineEnd)
		return
	}
	c.File.WriteString(">")
	if len(tunable.Value) > 0 {
		c.File.WriteString(tunable.Value)
		if c.AddReferences {
			c.addReference(tunable)
		}
	} else {
		c.File.WriteString(c.LineEnd)
		for _, t := range tunable.Tunables {
			c.writeTunable(t, depth+1)
		}
		c.indent(depth)
	}
	c.File.WriteString(fmt.Sprintf("</%v>%v", tunable.XMLName.Local, c.LineEnd))
}

// writeInstance writes out an instance or module at a specified indentation depth.
func (c *Context) writeInstance(instance combined.Instance, depth int) {
	c.indent(depth)
	c.File.WriteString("<")
	c.File.WriteString(instance.XMLName.Local)
	c.writeArgument("c", instance.Class)
	c.writeArgument("i", instance.Instance)
	c.writeArgument("m", instance.Module)
	c.writeArgument("n", instance.Name)
	c.writeArgument("s", instance.Id)
	c.File.WriteString(">")
	c.File.WriteString(c.LineEnd)
	for _, t := range instance.Tunables {
		c.writeTunable(t, depth+1)
	}
	c.indent(depth)
	c.File.WriteString(fmt.Sprintf("</%v>", instance.XMLName.Local))
}

// Write writes out an instance.
func (c *Context) Write(instance combined.Instance) error {
	if c.File == nil {
		return noFileError
	}
	c.File.WriteString(xmlHeader)
	c.File.WriteString(c.LineEnd)
	c.writeInstance(instance, 0)
	return nil
}
