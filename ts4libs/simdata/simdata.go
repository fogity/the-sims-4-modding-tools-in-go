// Package simdata handles reading and writing of simdata files.
// This file contains the API.
package simdata

import (
	"bytes"
	"fmt"
)

// Simdata is the main simdata representation.
type Simdata struct {
	objects map[string]*object
}

// schema is a simdata schema.
type schema struct {
	header  *schemaHeader
	columns []*column
	name    string
}

// column is a column in a schema.
type column struct {
	header *schemaColumn
	name   string
}

// table is a simdata table.
type table struct {
	header *tableInfo
	schema *schema
	start  int64
	name   string
}

// object is a simdata object.
type object struct {
	schema *schema
	values map[string]interface{}
	name   string
}

// Read reads simdata from bytes.
func Read(b []byte) (d *Simdata, e error) {
	defer func() {
		if r := recover(); r != nil {
			e = r.(error)
		}
	}()

	r := bytes.NewReader(b)

	c := new(readContext)

	c.r = r

	d = c.readSimdata()

	if len(d.objects) != 1 {
		e = fmt.Errorf("simdata with %v root objects not supported", len(d.objects))
	}

	return
}

// Write writes simdata to bytes.
func (d *Simdata) Write() (b []byte, e error) {
	defer func() {
		if r := recover(); r != nil {
			e = r.(error)
		}
	}()

	c := new(writeContext)

	c.d = d

	b = c.writeSimdata()

	return
}

// GetValues returns the values from the root object as a map.
func (d *Simdata) GetValues() map[string]interface{} {
	for _, object := range d.objects {
		return object.values
	}
	return nil
}

// GetValue returns the specified value from the root object.
func (d *Simdata) GetValue(name string) (interface{}, bool) {
	for _, object := range d.objects {
		val, ok := object.values[name]
		return val, ok
	}
	return nil, false
}

// SetValue sets the specified value for a field.
func (d *Simdata) SetValue(name string, value interface{}) error {
	for _, object := range d.objects {
		found := false
		for _, column := range object.schema.columns {
			if column.name == name {
				ok, err := checkType(value, int(column.header.DataType))
				if err != nil {
					return err
				}
				if !ok {
					return fmt.Errorf("value not of correct type")
				}
				found = true
				break
			}
		}
		if !found {
			return fmt.Errorf("object do not have the specified field")
		}
		object.values[name] = value
		return nil
	}
	return fmt.Errorf("no object found")
}

// checkType checks that value is of the given data type.
func checkType(value interface{}, dataType int) (bool, error) {
	switch dataType {
	case dtFloat:
		_, ok := value.(float32)
		return ok, nil
	default:
		return false, fmt.Errorf("data type (%v) not implemented", dataType)
	}
}
