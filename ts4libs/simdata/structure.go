// Package simdata handles reading and writing of simdata files.
// This file contains the simdata structure.
package simdata

import (
	"ts4libs/hash"
)

var (
	// The file type identifier.
	identifier = [4]byte{'D', 'A', 'T', 'A'}
	// The hash used for null strings.
	nullHash   = hash.Fnv32("")
)

const (
	// The supported file version.
	version = 0x101
	// The null offset.
	null    = int32(-0x7FFFFFFF) - 1
	// The value for the unused field.
	unused  = 0xFFFFFFFF
)

// The data type enum.
const (
	dtBool = iota
	dtChar8
	dtInt8
	dtUInt8
	dtInt16
	dtUInt16
	dtInt32
	dtUInt32
	dtInt64
	dtUInt64
	dtFloat
	dtString8
	dtHashedString8
	dtObject
	dtVector
	dtFloat2
	dtFloat3
	dtFloat4
	dtTableSetReference
	dtResourceKey
	dtLocKey
	dtUndefined
)

// Default offsets.
const (
	headerTableInfoOffset = int32(8)
	headerSchemaOffset    = int32(16)
)

// header is the simdata header.
type header struct {
	Identifier      [4]byte
	Version         uint32
	TableInfoOffset int32
	TableInfoCount  int32
	SchemaOffset    int32
	SchemaCount     int32
}

// Adjustments for relative offsets.
const (
	headerTableInfoAdjust = int64(16)
	headerSchemaAdjust    = int64(8)
)

// Default offsets.
const (
	tableInfoSchemaOffset = int32(8)
	tableInfoRowOffset    = int32(20)
)

// tableInfo contains information about the tables.
type tableInfo struct {
	NameOffset   int32
	NameHash     uint32
	SchemaOffset int32
	DataType     uint32
	RowSize      uint32
	RowOffset    int32
	RowCount     uint32
}

// Adjustments for relative offsets.
const (
	tableInfoNameAdjust   = int64(28)
	tableInfoSchemaAdjust = int64(20)
	tableInfoRowAdjust    = int64(8)
)

// Default offsets.
const (
	schemaHeaderColumnOffset = int32(16)
)

// schemaHeader contains information about the schemas.
type schemaHeader struct {
	NameOffset   int32
	NameHash     uint32
	SchemaHash   uint32
	SchemaSize   uint32
	ColumnOffset int32
	ColumnCount  uint32
}

// Adjustments for relative offsets.
const (
	schemaHeaderNameAdjust   = int64(24)
	schemaHeaderColumnAdjust = int64(8)
)

// Default offsets.
const (
	schemaColumnSchemaOffset = int32(24)
)

// schemaColumn is a column in a schema.
type schemaColumn struct {
	NameOffset   int32
	NameHash     uint32
	DataType     uint16
	Flags        uint16
	Offset       uint32
	SchemaOffset int32
}

// Adjustments for relative offsets.
const (
	schemaColumnNameAdjust   = int64(20)
	schemaColumnSchemaAdjust = int64(4)
)
