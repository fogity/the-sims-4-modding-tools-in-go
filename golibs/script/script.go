// Package script implements an extensible scripting language.
// This file handles running the scripts.
package script

import (
	"fmt"
	"io/ioutil"

	"golibs/script/context"
	"golibs/script/parse"
)

// Run interprets a script (as bytes) given a language definition.
func Run(def *context.Definitions, data []byte) (e error) {
	// Returns an error instead of crashing.
	defer func() {
		if r := recover(); r != nil {
			e = fmt.Errorf(r.(string))
		}
	}()

	// Parses the data.
	tree := parse.Parse(data)

	// Converts the tree into executable calls.
	calls := convert(def, tree)

	// Create the root context for the execution.
	context := context.NewContext(def)

	// Execute each statement in the script in turn.
	for _, call := range calls {
		err := call.Statement.Execute(context, call.Arguments)
		if err != nil {
			return err
		}
	}

	return nil
}

// RunFile opens and runs a script file given a path and a language definition.
func RunFile(def *context.Definitions, path string) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	return Run(def, data)
}
