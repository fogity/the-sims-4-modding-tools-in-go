// Package define contains some standard definitions for script languages.
// This file contains some helper functions.
package define

import (
	"fmt"
	"strings"

	"golibs/script/context"
	"golibs/script/parse"
)

// IsKeyword checks if a part matches a keyword string.
func IsKeyword(part parse.Part, str string) bool {
	if word, ok := part.(parse.Word); ok {
		if string(word) == str {
			return true
		}
	}
	return false
}

// IsName checks if a part is a base name.
func IsName(part parse.Part) bool {
	if word, ok := part.(parse.Word); ok {
		if !strings.ContainsAny(string(word), ".") {
			return true
		}
	}
	return false
}

// IsBlock checks if a part is a block.
func IsBlock(part parse.Part) bool {
	if _, ok := part.(parse.Block); ok {
		return true
	}
	return false
}

// IsExpression checks if a part is an expression.
func IsExpression(part parse.Part) bool {
	return !IsBlock(part)
}

// GetArgument extracts an actual value from an argument in a context.
func GetArgument(c *context.Context, arg interface{}) (interface{}, error) {
	switch a := arg.(type) {
	// Extracts the values of variables.
	case Name:
		if value, ok := c.Get(string(a)); ok {
			return value, nil
		}
		return nil, fmt.Errorf("variable '%v' not defined", a)
	// Executes expression calls.
	case context.ExpressionCall:
		value, err := a.Expression.Execute(c, a.Arguments)
		if err != nil {
			return nil, err
		}
		return value, nil
	// Extracts the values within lists.
	case []interface{}:
		for i, item := range a {
			argument, err := GetArgument(c, item)
			if err != nil {
				return nil, err
			}
			a[i] = argument
		}
		return a, nil
	// Otherwise the argument is already an actual value.
	default:
		return a, nil
	}
}

// GetName returns a name as a string.
func GetName(value interface{}) string {
	return string(value.(Name))
}
