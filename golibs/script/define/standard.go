// Package define contains some standard definitions for script languages.
// This file contains the definitions.
package define

import (
	"fmt"
	"strings"

	"golibs/script/context"
	"golibs/script/parse"
)

var (
	// Standard is the standard definitions.
	Standard = &context.Definitions{
		Values: map[string]interface{}{
			// The boolean literals are constants.
			"true":  true,
			"false": false,
		},
		Types: []*context.Type{
			List, Float32, Int64,
		},
		Statements: []*context.Statement{
			Skip, ForLoop, If, SetValue,
		},
		Expressions: []*context.Expression{
			GetValue, Equal, Any, Contains,
		},
	}

	// Int64 is the default integer type.
	Int64 = &context.Type{
		Name: "int",
		// Is represented by the golang int64 type.
		Match: func(value interface{}) bool {
			_, ok := value.(int64)
			return ok
		},
		// Captures all integer literals.
		Parse: func(s string) (interface{}, error) {
			var i int64
			_, err := fmt.Sscan(s, &i)
			return i, err
		},
	}

	// Float32 is the default floating point type.
	Float32 = &context.Type{
		Name: "float",
		// Is represented by the golang float32 type.
		Match: func(value interface{}) bool {
			_, ok := value.(float32)
			return ok
		},
		// Captures any float literal suffixed with an 'f'.
		Parse: func(s string) (interface{}, error) {
			var i float32
			if s[len(s)-1] != 'f' {
				return nil, fmt.Errorf("not a float")
			}
			_, err := fmt.Sscan(s[:len(s)-1], &i)
			return i, err
		},
	}

	// List is the default list type.
	List = &context.Type{
		Name: "list",
		// Is represented by golang lists.
		Match: func(value interface{}) bool {
			_, ok := value.([]interface{})
			return ok
		},
		// Is trivially a list type.
		List: func(list interface{}) ([]interface{}, error) {
			return list.([]interface{}), nil
		},
	}

	// ForLoop is a for loop statement.
	ForLoop = &context.Statement{
		Name:      "for",
		Match:     matchFor,
		Arguments: argsFor,
		Execute:   execFor,
	}

	// If is an if statement (without else).
	If = &context.Statement{
		Name:      "if",
		Match:     matchIf,
		Arguments: argsIf,
		Execute:   execIf,
	}

	// Skip is used in a for loop to skip to the next iteration.
	Skip = &context.Statement{
		Name:      "skip",
		Match:     matchSkip,
		Arguments: argsSkip,
		Execute:   execSkip,
	}

	// SetValue is an assignment statement.
	SetValue = &context.Statement{
		Name:      "set",
		Match:     matchSet,
		Arguments: argsSet,
		Execute:   execSet,
	}

	// GetValue is a variable lookup expression.
	GetValue = &context.Expression{
		Name:      "get",
		Match:     matchGet,
		Arguments: argsGet,
		Execute:   execGet,
	}

	// Negate is a logical negation expression.
	// Matched during parsing.
	Negate = &context.Expression{
		Name:    "negate",
		Execute: execNegate,
	}

	// Convert is a string parsing expression.
	// Matched during parsing.
	Convert = &context.Expression{
		Name:    "convert",
		Execute: execConvert,
	}

	// Equal is an equality checking expression.
	Equal = &context.Expression{
		Name:      "equal",
		Match:     matchEqual,
		Arguments: argsEqual,
		Execute:   execEqual,
	}

	// Any does list mapping and boolean 'or' folding.
	Any = &context.Expression{
		Name:      "any",
		Match:     matchAny,
		Arguments: argsAny,
		Execute:   execAny,
	}

	// Contains does substring finding.
	Contains = &context.Expression{
		Name:      "contains",
		Match:     matchContains,
		Arguments: argsContains,
		Execute:   execContains,
	}
)

// The type of variable names.
type Name string

// matchFor matches 'for <name> in <list> {..}'.
func matchFor(parts []parse.Part) bool {
	if len(parts) != 5 {
		return false
	}

	return IsKeyword(parts[0], "for") && IsName(parts[1]) && IsKeyword(parts[2], "in") && IsExpression(parts[3]) && IsBlock(parts[4])
}

// argsFor extracts the arguments.
func argsFor(parts []parse.Part) []parse.Part {
	return []parse.Part{parts[1], parts[3], parts[4]}
}

// execFor executes a for loop.
func execFor(c *context.Context, args []interface{}) error {
	name := GetName(args[0])

	lister, err := GetArgument(c, args[1])
	if err != nil {
		return err
	}
	list, err := c.Definitions().List(lister)
	if err != nil {
		return err
	}

	block := args[2].([]context.StatementCall)

loop:
	for _, item := range list {
		context := c.NewContext()
		context.Set(name, item)

		for _, call := range block {
			err := call.Statement.Execute(context, call.Arguments)
			if err != nil {
				return err
			}
			if context.GetFlag("break") {
				context.ResetFlag("break")
				break loop
			}
			if context.GetFlag("skip") {
				context.ResetFlag("skip")
				break
			}
		}
	}

	return nil
}

// matchIf matches 'if <exp> {..}'.
func matchIf(parts []parse.Part) bool {
	if len(parts) != 3 {
		return false
	}

	return IsKeyword(parts[0], "if") && IsExpression(parts[1]) && IsBlock(parts[2])
}

// argsIf extracts the arguments.
func argsIf(parts []parse.Part) []parse.Part {
	return []parse.Part{parts[1], parts[2]}
}

// execIf executes and if statement.
func execIf(c *context.Context, args []interface{}) error {
	predicate, err := GetArgument(c, args[0])
	if err != nil {
		return err
	}

	block := args[1].([]context.StatementCall)

	val, ok := predicate.(bool)

	if !ok {
		return fmt.Errorf("expected boolean value")
	}

	if !val {
		return nil
	}

	context := c.NewContext()

	for _, call := range block {
		err := call.Statement.Execute(context, call.Arguments)
		if err != nil {
			return err
		}
	}

	return nil
}

// matchSkip matches 'skip'.
func matchSkip(parts []parse.Part) bool {
	if len(parts) != 1 {
		return false
	}

	return IsKeyword(parts[0], "skip")
}

// argsSkip is a dummy.
func argsSkip(parts []parse.Part) []parse.Part {
	return nil
}

// execSkip sets the skip flag.
func execSkip(c *context.Context, args []interface{}) error {
	c.SetFlag("skip")
	return nil
}

// matchSet matches 'set <var> to <val>'.
func matchSet(parts []parse.Part) bool {
	if len(parts) != 4 {
		return false
	}

	return IsKeyword(parts[0], "set") && IsExpression(parts[1]) && IsKeyword(parts[2], "to") && IsExpression(parts[3])
}

// argsSet extracts the arguments.
func argsSet(parts []parse.Part) []parse.Part {
	return []parse.Part{parts[1], parts[3]}
}

// execSet sets a variable to the given value.
func execSet(c *context.Context, args []interface{}) error {
	name := GetName(args[0])
	value, err := GetArgument(c, args[1])
	if err != nil {
		return err
	}

	c.Set(name, value)

	return nil
}

// matchGet matches any variable name.
func matchGet(parts []parse.Part) bool {
	if len(parts) != 1 {
		return false
	}

	return IsName(parts[0])
}

// argsGet just returns the input.
func argsGet(parts []parse.Part) []parse.Part {
	return parts
}

// execGet performs a variable lookup.
func execGet(c *context.Context, args []interface{}) (interface{}, error) {
	name := GetName(args[0])

	value, ok := c.Get(name)

	if !ok {
		return nil, fmt.Errorf("variable '%v' not defined", name)
	}

	return value, nil
}

// execNegate negates a boolean value.
func execNegate(c *context.Context, args []interface{}) (interface{}, error) {
	value, err := GetArgument(c, args[0])
	if err != nil {
		return nil, err
	}

	v, ok := value.(bool)
	if !ok {
		return nil, fmt.Errorf("expected boolean value")
	}

	return !v, nil
}

// execConvert parses a string as a type.
func execConvert(c *context.Context, args []interface{}) (interface{}, error) {
	value, err := GetArgument(c, args[0])
	if err != nil {
		return nil, err
	}

	v, ok := value.(string)
	if !ok {
		return nil, fmt.Errorf("expected string value")
	}

	for _, t := range c.Definitions().Types {
		if t.Parse == nil {
			continue
		}

		val, err := t.Parse(string(v))
		if err == nil {
			return val, nil
		}
	}

	return Name(v), nil
}

// matchEqual matches '<exp> = <exp>'.
func matchEqual(parts []parse.Part) bool {
	if len(parts) != 3 {
		return false
	}

	return IsExpression(parts[0]) && IsKeyword(parts[1], "=") && IsExpression(parts[2])
}

// argsEqual extracts the arguments.
func argsEqual(parts []parse.Part) []parse.Part {
	return []parse.Part{parts[0], parts[2]}
}

// execEqual checks if two values are equal (using golang equality).
func execEqual(c *context.Context, args []interface{}) (interface{}, error) {
	a, err := GetArgument(c, args[0])
	if err != nil {
		return nil, err
	}

	b, err := GetArgument(c, args[1])
	if err != nil {
		return nil, err
	}

	return a == b, nil
}

// matchAny matches '[parts] any <list>'
func matchAny(parts []parse.Part) bool {
	if len(parts) < 3 {
		return false
	}

	return IsKeyword(parts[len(parts)-2], "any") && IsExpression(parts[len(parts)-1])
}

// argsAny extracts and processes the arguments.
func argsAny(parts []parse.Part) []parse.Part {
	// Creates a new part list.
	exp := make([]parse.Part, 0)
	// Adds all parts preceding the 'any' keyword.
	for _, part := range parts[:len(parts)-2] {
		exp = append(exp, part)
	}
	// Append a special variable for the iteration.
	exp = append(exp, parse.Word("!any"))
	return []parse.Part{parse.Group(exp), parts[len(parts)-1]}
}

// execAny executes the any expression as a for loop.
func execAny(c *context.Context, args []interface{}) (interface{}, error) {
	exp := args[0]

	lister, err := GetArgument(c, args[1])
	if err != nil {
		return nil, err
	}
	list, err := c.Definitions().List(lister)
	if err != nil {
		return nil, err
	}

	for _, item := range list {
		c.Set("!any", item)

		predicate, err := GetArgument(c, exp)
		if err != nil {
			return nil, err
		}

		p, ok := predicate.(bool)
		if !ok {
			return nil, fmt.Errorf("expected boolean value")
		}

		if p {
			return true, nil
		}
	}

	return false, nil
}

// matchContains matches '<exp> contains <exp>'.
func matchContains(parts []parse.Part) bool {
	if len(parts) != 3 {
		return false
	}

	return IsExpression(parts[0]) && IsKeyword(parts[1], "contains") && IsExpression(parts[2])
}

// argsContains extracts the arguments.
func argsContains(parts []parse.Part) []parse.Part {
	return []parse.Part{parts[0], parts[2]}
}

// execContains performs substring finding.
func execContains(c *context.Context, args []interface{}) (interface{}, error) {
	a, err := GetArgument(c, args[0])
	if err != nil {
		return nil, err
	}
	as, ok := a.(string)
	if !ok {
		return nil, fmt.Errorf("expected string value")
	}

	b, err := GetArgument(c, args[1])
	if err != nil {
		return nil, err
	}
	bs, ok := b.(string)
	if !ok {
		return nil, fmt.Errorf("expected string value")
	}

	return strings.Contains(as, bs), nil
}
