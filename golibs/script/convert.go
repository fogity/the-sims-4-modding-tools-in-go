// Package script implements an extensible scripting language.
// This file converts a parse tree into interpretable calls.
package script

import (
	"golibs/script/context"
	"golibs/script/define"
	"golibs/script/parse"
)

// convert converts a tree into a list of statement calls.
func convert(def *context.Definitions, tree parse.Tree) []context.StatementCall {
	return convertBlock(def, parse.Block(tree))
}

// convertBlock converts a block into a list of statement calls.
func convertBlock(def *context.Definitions, block parse.Block) []context.StatementCall {
	calls := make([]context.StatementCall, len(block))

	for i, stmt := range block {
		ok := false

		for _, s := range def.Statements {
			if !s.Match(stmt) {
				continue
			}

			arguments := s.Arguments(stmt)

			args := make([]interface{}, len(arguments))

			for k, arg := range arguments {
				args[k] = convertArgument(def, arg)
			}

			call := context.StatementCall{
				Statement: s,
				Arguments: args,
			}

			calls[i] = call

			ok = true
			break
		}

		if !ok {
			panic("could not match statement")
		}
	}

	return calls
}

// convertArgument converts a part into a value.
func convertArgument(def *context.Definitions, arg parse.Part) interface{} {
	switch a := arg.(type) {
	case parse.Word:
		return convertWord(def, a)
	case parse.String:
		return string(a)
	case parse.Negate:
		op := convertArgument(def, a.Operand)
		return context.ExpressionCall{
			Expression: define.Negate,
			Arguments:  []interface{}{op},
		}
	case parse.Convert:
		op := convertArgument(def, a.Operand)
		return context.ExpressionCall{
			Expression: define.Convert,
			Arguments:  []interface{}{op},
		}
	case parse.List:
		items := make([]interface{}, len(a))
		for i, exp := range a {
			items[i] = convertArgument(def, exp)
		}
		return items
	case parse.Map:
		mp := make(map[string]interface{})
		for k, exp := range a {
			mp[k] = convertArgument(def, exp)
		}
		return mp
	case parse.Group:
		return convertExpression(def, a)
	case parse.Block:
		return convertBlock(def, a)
	default:
		panic("unknown argument type")
	}
}

// convertWord converts a word into either a type literal or a variable name.
func convertWord(def *context.Definitions, word parse.Word) interface{} {
	for _, t := range def.Types {
		if t.Parse == nil {
			continue
		}
		v, err := t.Parse(string(word))
		if err == nil {
			return v
		}
	}
	return define.Name(word)
}

// convertExpression converts a group into an expression call.
func convertExpression(def *context.Definitions, group parse.Group) context.ExpressionCall {
	for _, e := range def.Expressions {
		if !e.Match(group) {
			continue
		}

		arguments := e.Arguments(group)

		args := make([]interface{}, len(arguments))

		for k, arg := range arguments {
			args[k] = convertArgument(def, arg)
		}

		return context.ExpressionCall{
			Expression: e,
			Arguments:  args,
		}
	}

	panic("could not match expression")
}
