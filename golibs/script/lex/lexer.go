// Package lex handles lexing for the script language.
// This file is the lexer.
package lex

import (
	"bytes"
	"fmt"
)

// Lexer is the lexing state.
type Lexer struct {
	data []byte
	pos  int
}

// NewLexer creates a new lexer.
func NewLexer(data []byte) *Lexer {
	l := &Lexer{
		data: data,
		pos:  0,
	}

	l.trim()

	return l
}

// curr returns the current character.
func (l *Lexer) curr() byte {
	return l.data[l.pos]
}

// trim removes any preceding whitespace.
func (l *Lexer) trim() {
	for l.pos < len(l.data) {
		switch l.curr() {
		case ' ', '\t', '\r':
			l.pos++
		default:
			return
		}
	}
}

// Next extracts the next token.
func (l *Lexer) Next() Token {
	if l.pos >= len(l.data) {
		return EndOfFile{}
	}

	switch l.curr() {
	case '\n':
		l.pos++
		l.trim()
		return EndOfStatement{}
	case '#':
		i := bytes.IndexByte(l.data[l.pos:], '\n')
		if i == -1 {
			l.pos = len(l.data)
		} else {
			l.pos += i + 1
		}
		l.trim()
		return Comment{}
	case '"':
		i := bytes.IndexByte(l.data[l.pos+1:], '"')
		if i == -1 {
			panic("did not find matching \" when parsing string")
		}
		str := l.data[l.pos+1 : l.pos+i+1]
		l.pos += i + 2
		l.trim()
		return String(str)
	case '(':
		l.pos++
		l.trim()
		return StartOfGroup{}
	case ')':
		l.pos++
		l.trim()
		return EndOfGroup{}
	case '{':
		l.pos++
		l.trim()
		return StartOfBlock{}
	case '}':
		l.pos++
		l.trim()
		return EndOfBlock{}
	case '[':
		l.pos++
		l.trim()
		return StartOfList{}
	case ']':
		l.pos++
		l.trim()
		return EndOfList{}
	case ',':
		l.pos++
		l.trim()
		return EndOfItem{}
	case '<':
		l.pos++
		l.trim()
		return StartOfMap{}
	case '>':
		l.pos++
		l.trim()
		return EndOfMap{}
	case ':':
		l.pos++
		l.trim()
		return EndOfKey{}
	case '!':
		l.pos++
		l.trim()
		return Negate{}
	case '@':
		l.pos++
		l.trim()
		return Convert{}
	default:
		word := l.extractWord()
		l.trim()
		return Word(word)
	}
}

// extractWord extracts a word (a string of characters not containing special characters).
func (l *Lexer) extractWord() string {
	start := l.pos

loop:
	for {
		if l.pos >= len(l.data) {
			break
		}

		switch l.curr() {
		case ' ', '\t', '\r', '\n', ')', '}', ']', ',', '>', ':':
			break loop
		case '#', '"', '(', '{', '[', '!', '@':
			word := string(l.data[start : l.pos+1])
			panic(fmt.Sprintf("reserved character found in word '%v'", word))
		}

		l.pos++
	}

	return string(l.data[start:l.pos])
}
