// Package lex handles lexing for the script language.
// This file defines the tokens.
package lex

// Any type can be a token.
type Token interface{}

// The token for end of the script.
type EndOfFile struct{}

// The token for start of a block of statements.
type StartOfBlock struct{}

// The token for end of a block.
type EndOfBlock struct{}

// The token for start of a group (corresponding to an expression).
type StartOfGroup struct{}

// The token for end of a group.
type EndOfGroup struct{}

// The token for end of a statement (between statements).
type EndOfStatement struct{}

// The token for start of a list.
type StartOfList struct{}

// The token for end of a list.
type EndOfList struct{}

// The token for end of an item (between items in a list or map).
type EndOfItem struct{}

// The token for start of a map.
type StartOfMap struct{}

// The token for end of a map.
type EndOfMap struct{}

// The token for end of a key (within a key value pair in a map).
type EndOfKey struct{}

// The token for comments (without the actual comment).
type Comment struct{}

// The token for logical negation.
type Negate struct{}

// The token for conversion of string literals.
type Convert struct{}

// The type of word tokens.
type Word string

// The type of string literal tokens.
type String string
