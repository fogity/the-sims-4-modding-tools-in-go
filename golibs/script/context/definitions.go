// Package context handles the context and language when interpreting a script.
// This file handles the language.
package context

import (
	"fmt"

	"golibs/script/parse"
)

// Definitions define a script language.
type Definitions struct {
	// A map defining the language constants.
	Values      map[string]interface{}
	// A list of type definitions.
	Types       []*Type
	// A list of statement definitions.
	Statements  []*Statement
	// A list of expression definitions.
	Expressions []*Expression
}

// List returns the list generated from a value if it is of a list type.
func (d *Definitions) List(value interface{}) ([]interface{}, error) {
	for _, t := range d.Types {
		if !t.Match(value) {
			continue
		}

		if t.List == nil {
			return nil, fmt.Errorf("value type is not a list")
		}

		return t.List(value)
	}
	return nil, fmt.Errorf("value type is not a list")
}

// Type defines a language type.
type Type struct {
	// The name of the type.
	Name  string
	// A function that determines if a value is of this type.
	Match func(interface{}) bool
	// A function for parsing a value of this type.
	Parse func(string) (interface{}, error)
	// A function for getting an attribute of this type.
	Get   func(interface{}, string) (interface{}, error)
	// A function for setting an attribute of this type.
	Set   func(interface{}, string, interface{}) error
	// A function for generating a list from a value of this type.
	List  func(interface{}) ([]interface{}, error)
}

// Statement defines a language statement.
type Statement struct {
	// The name of the statement.
	Name      string
	// A function for checking if a list of tokens match the statement.
	Match     func([]parse.Part) bool
	// A function for extracting the arguments from a list of matched tokens.
	Arguments func([]parse.Part) []parse.Part
	// A function implementing the execution of the statment.
	Execute   func(*Context, []interface{}) error
}

// Expression defines a language expression.
type Expression struct {
	// The name of the expression.
	Name      string
	// A function for checking if a list of tokens match the expression.
	Match     func([]parse.Part) bool
	// A function for extracting the arguments from a list of matched tokens.
	Arguments func([]parse.Part) []parse.Part
	// A function implementing the execution of the expression.
	Execute   func(*Context, []interface{}) (interface{}, error)
}

// StatementCall stores an invokation of a statement.
type StatementCall struct {
	// The statement to invoke.
	Statement *Statement
	// The arguments.
	Arguments []interface{}
}

// ExpressionCall stores an invokation of an expression.
type ExpressionCall struct {
	// The expression to invoke.
	Expression *Expression
	// The arguments.
	Arguments  []interface{}
}
