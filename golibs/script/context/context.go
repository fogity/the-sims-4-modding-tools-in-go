// Package context handles the context and language when interpreting a script.
// This file handles the context.
package context

import "strings"

// Context is the context used for interpreting a script.
type Context struct {
	// A pointer to the definition of the script language.
	definitions *Definitions
	// A pointer to the enclosing context.
	parent      *Context
	// A map containing the values defined in this context.
	values      map[string]interface{}
	// A map containing the state flags for this context.
	// It is only used by the root context.
	flags       map[string]bool
}

// NewContext creates a new context given a language definition.
func NewContext(d *Definitions) *Context {
	c := new(Context)
	c.definitions = d
	c.values = make(map[string]interface{})
	// Inserts the language constants into the context.
	for name, value := range d.Values {
		c.values[name] = value
	}
	c.flags = make(map[string]bool)
	return c
}

// Definitions returns the language definition from a context.
func (c *Context) Definitions() *Definitions {
	return c.definitions
}

// GetFlag checks the given flag for the root context.
func (c *Context) GetFlag(name string) bool {
	if c.parent != nil {
		return c.parent.GetFlag(name)
	}
	flag, ok := c.flags[name]
	if ok {
		return flag
	}
	return false
}

// SetFlag sets the given flag for the root context.
func (c *Context) SetFlag(name string) {
	if c.parent != nil {
		c.parent.SetFlag(name)
		return
	}
	c.flags[name] = true
}

// ResetFlag resets the given flag for the root context.
func (c *Context) ResetFlag(name string) {
	if c.parent != nil {
		c.parent.ResetFlag(name)
	}
	c.flags[name] = false
}

// NewContext creates a child context.
func (c *Context) NewContext() *Context {
	context := new(Context)
	context.definitions = c.definitions
	context.parent = c
	context.values = make(map[string]interface{})
	context.flags = make(map[string]bool)
	return context
}

// Get returns the value of a given variable in the context.
func (c *Context) Get(name string) (interface{}, bool) {
	// Splits the variable name into a base name and attributes.
	parts := strings.Split(name, ".")
	v, ok := c.values[parts[0]]
	if !ok {
		if c.parent != nil {
			return c.parent.Get(name)
		}
		return nil, false
	}
	if len(parts) > 1 {
		return c.getAttr(v, parts[1:])
	}
	return v, true
}

// Set assigns a given value to a given variable in the context.
func (c *Context) Set(name string, value interface{}) {
	// Splits the variable name into a base name and attributes.
	parts := strings.Split(name, ".")
	if len(parts) > 1 {
		v, ok := c.Get(parts[0])
		if !ok {
			panic("variable not found")
		}
		c.setAttr(v, parts[1:], value)
		return
	}
	c.values[parts[0]] = value
}

// getAttr returns a given attribute of a given value.
// Takes a chain of attributes.
func (c *Context) getAttr(v interface{}, parts []string) (interface{}, bool) {
	for _, t := range c.definitions.Types {
		if !t.Match(v) {
			continue
		}

		if t.Get == nil {
			panic("type does not have gettable attributes")
		}

		value, err := t.Get(v, parts[0])

		if err != nil {
			panic(err.Error())
		}

		if len(parts) > 1 {
			return c.getAttr(value, parts[1:])
		}

		return value, true
	}

	panic("could not find type")
}

// setAttr assigns a given value to a given attribute for a given value.
// Takes a chain of attributes.
func (c *Context) setAttr(v interface{}, parts []string, value interface{}) {
	for _, t := range c.definitions.Types {
		if !t.Match(v) {
			continue
		}

		if len(parts) > 1 {
			if t.Get == nil {
				panic("type does not have gettable attributes")
			}

			nv, err := t.Get(v, parts[0])

			if err != nil {
				panic(err.Error())
			}

			c.setAttr(nv, parts[1:], value)

			return
		}

		if t.Set == nil {
			panic("type does not have settable attributes")
		}

		err := t.Set(v, parts[0], value)

		if err != nil {
			panic(err.Error())
		}

		return
	}

	panic("could not find type")
}
