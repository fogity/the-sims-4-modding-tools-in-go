// Package parse builds a parse tree from tokens.
// This file is the builder.
package parse

import (
	"fmt"

	"golibs/script/lex"
)

// Parse creates a parse tree from an array of bytes.
func Parse(data []byte) Tree {
	l := lex.NewLexer(data)

	block, _ := parseBlock(l, true)

	return Tree(block)
}

// parseBlock builds a block.
// Second argument specifies if it is the root block or not.
// Second return is the termination token.
func parseBlock(l *lex.Lexer, top bool) (Block, lex.Token) {
	stmts := make([]Statement, 0)

	for {
		stmt, ok, token := parseStatement(l)

		if ok {
			stmts = append(stmts, stmt)
		}

		switch token.(type) {
		case lex.EndOfFile:
			if top {
				return Block(stmts), token
			}
		case lex.EndOfBlock:
			if !top {
				return Block(stmts), token
			}
		}
	}
}

// parseStatement builds a statement.
// Second return is false if the statement is empty.
// Third return is the termination token.
func parseStatement(l *lex.Lexer) (Statement, bool, lex.Token) {
	stmt := make([]Part, 0)

	for {
		part, ok, token := parsePart(l)

		if ok {
			stmt = append(stmt, part)
		} else {
			switch token.(type) {
			case lex.EndOfBlock:
				if len(stmt) == 0 {
					return nil, false, token
				}
				return Statement(stmt), true, token
			}
		}

		switch token.(type) {
		case lex.EndOfFile, lex.EndOfStatement, lex.Comment:
			if len(stmt) == 0 {
				return nil, false, token
			}
			return Statement(stmt), true, token
		}
	}
}

// parsePart builds a part.
// Second return is false if the part is empty.
// Third return is the termination token.
func parsePart(l *lex.Lexer) (Part, bool, lex.Token) {
	token := l.Next()

	switch t := token.(type) {
	case lex.Comment, lex.EndOfStatement, lex.EndOfBlock, lex.EndOfGroup, lex.EndOfFile:
		return nil, false, token
	case lex.StartOfBlock:
		block, tkn := parseBlock(l, false)
		return block, true, tkn
	case lex.StartOfGroup:
		group, tkn := parseGroup(l)
		return group, true, tkn
	case lex.StartOfList:
		list, tkn := parseList(l)
		return list, true, tkn
	case lex.StartOfMap:
		mp, tkn := parseMap(l)
		return mp, true, tkn
	case lex.Negate:
		part, ok, tkn := parsePart(l)
		if !ok {
			panic("illegal expression after '!'")
		}
		return Negate{part}, true, tkn
	case lex.Convert:
		part, ok, tkn := parsePart(l)
		if !ok {
			panic("illegal expression after '@'")
		}
		return Convert{part}, true, tkn
	case lex.Word:
		return Word(t), true, token
	case lex.String:
		return String(t), true, token
	default:
		panic(fmt.Sprintf("unknown token '%T'", t))
	}
}

// parseGroup builds a group.
// Second return is the termination token.
func parseGroup(l *lex.Lexer) (Group, lex.Token) {
	group := make([]Part, 0)

	for {
		part, ok, token := parsePart(l)

		if ok {
			group = append(group, part)
		}

		switch token.(type) {
		case lex.EndOfGroup:
			return Group(group), token
		}
	}
}

// parseList builds a list.
// Second return is the termination token.
func parseList(l *lex.Lexer) (List, lex.Token) {
	list := make([]Part, 0)

	for {
		part, ok, token := parsePart(l)

		if ok {
			list = append(list, part)
		}

		switch token.(type) {
		case lex.EndOfList:
			return List(list), token
		case lex.EndOfItem:
			continue
		}

		token = l.Next()
		switch token.(type) {
		case lex.EndOfList:
			return List(list), token
		case lex.EndOfItem:
		default:
			panic("expected ',' after item in list")
		}
	}
}

// parseMap builds a map.
// Second return is the termination token.
func parseMap(l *lex.Lexer) (Map, lex.Token) {
	mp := make(map[string]Part)

	for {
		part, ok, token := parsePart(l)

		if ok {
			key, ok := part.(Word)
			if !ok {
				panic("expected name before ':' in map")
			}

			_, ok = l.Next().(lex.EndOfKey)
			if !ok {
				panic("expected ':' after name in map")
			}

			part, ok, token = parsePart(l)
			if !ok {
				panic("expected expression after ':' in map")
			}

			mp[string(key)] = part
		}

		switch token.(type) {
		case lex.EndOfMap:
			return Map(mp), token
		case lex.EndOfItem:
			continue
		}

		token = l.Next()
		switch token.(type) {
		case lex.EndOfMap:
			return Map(mp), token
		case lex.EndOfItem:
		default:
			panic("expected ',' after entry in map")
		}
	}
}
