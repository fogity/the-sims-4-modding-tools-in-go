// Package parse builds a parse tree from tokens.
// This file defines the tree.
package parse

// The root of a parse tree is a block.
type Tree Block

// A block is a list of statements.
type Block []Statement

// A statement consists of a group.
type Statement Group

// A list consists of a group.
type List Group

// A map is a map of names mapped to parts.
type Map map[string]Part

// A group is a list of parts.
type Group []Part

// A part can be any type.
type Part interface{}

// Negate is a node with a child operand.
type Negate struct {
	Operand Part
}

// Convert is a node with a child operand.
type Convert struct {
	Operand Part
}

// The type of words.
type Word string

// The type of string literals.
type String string
