# The Sims 4 Modding Tools Written in Go.

This is a set of libraries and tools written in Go for extracting resources from
and creating modifications for The Sims 4.

The tools are sadly outdated at this point as the game data formats has been
updated and changed.

The rest of this readme will outline the different libraries and tools within
the project.

## The Scripting Library

The golibs folder contains a Go library that implements a generic scripting
language that can be extended into a domain specific language.

The language is only specified by the code and the behaviour of the interpreter.
For example use see the ts4libs and ts4mods.

## The Sims 4 Modding Library

The ts4libs folder contains a Go library for extracting, editing and packaging
resourses for The Sims 4. It also contains an extension for the previously
mentioned scripting language for writing modding scripts.

The library supports editing data packages (dbpf or package files) and some game
resources (caspart, simdata, stbl, xml tuning).

It also supports converting a jpeg with transparency metadata into a png.

## The Sims 4 Mods

The ts4mods folder contains two example mods written in this domain specific
scripting language. They showcase what the libraries can do and has comments
explaining what they do.

## The Sims 4 Tools

The ts4tools folder contains a set of programs that could be used to research
or make modifications to The Sims 4.

The engine program can be used to run a script made in the domain specific
language. It is a command line tool.

The moddertoolbox program can be used to calculate FNV-1a hashes of strings
and to convert between decimal and hexadecimal. It has a GUI made using QML.

The testertoolbox progam can be used to extract the thumbnails for casparts
and to extract xml tuning, strings and caspart names from the game.
It also has a GUI made using QML.
